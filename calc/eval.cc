#include "eval.h"

#include <iostream>
#include <cassert>

namespace calc {

int Eval::eval(std::shared_ptr<Ast> ast)
{
    if (!ast) {
        return 0;
    }

    switch (ast->type) {
        case Type::number:
            return std::static_pointer_cast<Number>(ast)->value;
        case Type::binary: {
            auto op = std::static_pointer_cast<Binary>(ast);
            int lhs = eval(op->lhs);
            int rhs = eval(op->rhs);
            auto get_op = [&] () -> int {
                switch (op->op) {
                    case Opcode::plus: return lhs + rhs;
                    case Opcode::minus: return lhs - rhs;
                    case Opcode::mul: return lhs * rhs;
                    case Opcode::div: return lhs / rhs;
                    default: assert(0);
                }
            };
            return get_op();
        }
        case Type::unary: {
            auto op = std::static_pointer_cast<Unary>(ast);
            auto get_op = [&] () -> int {
                switch (op->op) {
                    case Opcode::uminus: return -eval(op->operand);
                    default: assert(0);
                }
            };
            return get_op();
        }
        default: {
            assert(0);
        }
    }
}

}
