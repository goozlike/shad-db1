#include "comparator.h"

#include <cassert>
#include <cstdint>
#include <string>
#include <variant>

namespace shdb
{

int16_t Comparator::operator()(const Row & lhs, const Row & rhs) const
{
    (void)(lhs);
    (void)(rhs);
    throw std::runtime_error("Not implemented");
}

int16_t compareRows(const Row & lhs, const Row & rhs)
{
    return Comparator()(lhs, rhs);
}

int16_t compareValue(const Value & lhs, const Value & rhs)
{
    (void)(lhs);
    (void)(rhs);
    throw std::runtime_error("Not implemented");
}
}
