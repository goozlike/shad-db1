#pragma once

#include <vector>
#include <stdexcept>

namespace shdb
{

template <class Key, class Value>
class ClockCache
{
public:
    explicit ClockCache(std::vector<Value> free_values)
    {
        (void)(free_values);
        throw std::runtime_error("Not implemented");
    }

    std::pair<bool, Value> find(const Key & key)
    {
        (void)(key);
        throw std::runtime_error("Not implemented");
    }

    Value put(const Key & key)
    {
        (void)(key);
        throw std::runtime_error("Not implemented");
    }

    void lock(const Key & key)
    {
        (void)(key);
        throw std::runtime_error("Not implemented");
    }

    void unlock(const Key & key)
    {
        (void)(key);
        throw std::runtime_error("Not implemented");
    }

    // Your code goes here.
};

}
