#include "interpreter.h"

#include "accessors.h"
#include "ast.h"
#include "ast_visitor.h"
#include "executor.h"
#include "expression.h"
#include "lexer.h"
#include "parser.hpp"
#include "row.h"

namespace shdb
{

Interpreter::Interpreter(std::shared_ptr<Database> db_) : db(std::move(db_))
{
    registerAggregateFunctions(aggregate_function_factory);
}

RowSet Interpreter::execute(const std::string & query)
{
    Lexer lexer(query.c_str(), query.c_str() + query.size());
    ASTPtr result;
    std::string error;
    shdb::Parser parser(lexer, result, error);
    parser.parse();
    if (!result || !error.empty())
        throw std::runtime_error("Bad input: " + error);

    switch (result->type)
    {
        case ASTType::selectQuery:
            return executeSelect(std::static_pointer_cast<ASTSelectQuery>(result));
        case ASTType::insertQuery:
            executeInsert(std::static_pointer_cast<ASTInsertQuery>(result));
            break;
        case ASTType::createQuery:
            executeCreate(std::static_pointer_cast<ASTCreateQuery>(result));
            break;
        case ASTType::dropQuery:
            executeDrop(std::static_pointer_cast<ASTDropQuery>(result));
            break;
        default:
            throw std::runtime_error("Invalid AST. Expected SELECT, INSERT, CREATE or DROP query");
    }

    return RowSet{};
}

RowSet Interpreter::executeSelect(const ASTSelectQueryPtr & select_query_ptr)
{
    (void)(select_query_ptr);
    throw std::runtime_error("Not implemented");
}

void Interpreter::executeInsert(const std::shared_ptr<ASTInsertQuery> & insert_query)
{
    (void)(insert_query);
    throw std::runtime_error("Not implemented");
}

void Interpreter::executeCreate(const ASTCreateQueryPtr & create_query)
{
    (void)(create_query);
    throw std::runtime_error("Not implemented");
}

void Interpreter::executeDrop(const ASTDropQueryPtr & drop_query)
{
    (void)(drop_query);
    throw std::runtime_error("Not implemented");
}

}
